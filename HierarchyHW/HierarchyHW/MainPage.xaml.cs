﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HierarchyHW
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void OnDinoButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1());
        }
        async void OnVolcanoButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2());
        }
        async void OnPlagueButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page3());
        }
        async void OnWarButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page4());
        }
        async void OnSafeButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page5());
        }

    } 

}
