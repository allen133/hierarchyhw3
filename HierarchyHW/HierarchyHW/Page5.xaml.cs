﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HierarchyHW
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page5 : ContentPage
    {
        public Page5()
        {
            InitializeComponent();
        }

        async void OnNextPageClicked(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }

        async void OnBackPageClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}