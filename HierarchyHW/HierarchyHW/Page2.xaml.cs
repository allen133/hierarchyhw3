﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HierarchyHW
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();
        }

        async void OnNextPageClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page3());
        }
        async void OnBackPageClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}